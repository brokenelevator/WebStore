# WebStore
This is unfinished project for Java Programming Workshop.  
Simple web store created with Spring Boot. Tested with JUnit.

#### Build and package
```bash
mvn package
```

#### Run
```bash
java -jar target/WebStore-0.0.1-SNAPSHOT.jar
```
Go to <localhost:8080>

## Note
Uses Maven for compiling, testing and packaging automation.  
Requires Java 1.8.
